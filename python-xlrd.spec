%global _empty_manifest_terminate_build 0
Name:		python-xlrd
Version:	2.0.1
Release:	1
Summary:	Library for developers to extract data from Microsoft Excel (tm) spreadsheet files
License:	BSD-3-Clause
URL:		http://www.python-excel.org/
Source0:	https://files.pythonhosted.org/packages/a6/b3/19a2540d21dea5f908304375bd43f5ed7a4c28a370dc9122c565423e6b44/xlrd-2.0.1.tar.gz
BuildArch:	noarch


%description
Extract data from Excel spreadsheets (.xls and .xlsx, versions 2.0 onwards) on any platform. Pure Python (2.7, 3.4+). Strong support for Excel dates. Unicode-aware.




%package -n python3-xlrd
Summary:	Library for developers to extract data from Microsoft Excel (tm) spreadsheet files
Provides:	python-xlrd
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-xlrd
Extract data from Excel spreadsheets (.xls and .xlsx, versions 2.0 onwards) on any platform. Pure Python (2.7, 3.4+). Strong support for Excel dates. Unicode-aware.




%package help
Summary:	Development documents and examples for xlrd
Provides:	python3-xlrd-doc
%description help
Extract data from Excel spreadsheets (.xls and .xlsx, versions 2.0 onwards) on any platform. Pure Python (2.7, 3.4+). Strong support for Excel dates. Unicode-aware.




%prep
%autosetup -n xlrd-2.0.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-xlrd -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Jun 23 2022 SimpleUpdate Robot <tc@openeuler.org> - 2.0.1-1
- Upgrade to version 2.0.1

* Wed May 11 2022 yangping <yangping69@h-partners> - 1.2.0-2
- License compliance rectification

* Thu Aug 13 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
